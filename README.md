### NO LONGER IN USE.

Bot is now in https://gitlab.com/gitlab-com/request-for-help

# RFH Triage

This project houses the [Triage Gem](https://gitlab.com/gitlab-org/ruby/gems/gitlab-triage) that is used for triaging [Request for Help issues](https://handbook.gitlab.com/handbook/support/workflows/how-to-get-help/#how-to-use-gitlabcom-to-formally-request-help-from-the-gitlab-development-team).


[Support Team Meta issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/5390)
